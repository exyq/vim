"++++++ BaseSets ++++++++++++++
syntax on
set number
set cursorline
set ruler
set relativenumber
set wrap
set showcmd
set wildmenu
set hlsearch
set incsearch
set smartcase
set scrolloff=5
set mouse=""
set shortmess=atI
set helplang=cn
"au BufWinEnter * silent! set autochdir

"++++++ GVimConfig ++++++++++++
set guifont=Consolas:i:h12
silent! set guifontwide=幼圆:b:h12:cGB2312
set guioptions-=m
set guioptions-=T
set guioptions-=r
set guioptions-=R

"++++++ VimFiles ++++++++++++++
set undofile
set backup
set undodir=$VIM/.vim/undo
set backupdir=$VIM/.vim/backup
set directory=$VIM/.vim/swap
set viminfo+=<200,'200,/100,:500,h,s20,n$VIM/.vim/_viminfo

"++++++ Codings +++++++++++++++
set langmenu=zh_CN.UTF-8
set helplang=cn
set termencoding=utf-8
set encoding=utf-8
set fileencodings=utf8,ucs-bom,gbk,cp936,gb2312,gb18030

"++++++ Indent ++++++++++++++++
set backspace=indent,eol,start
set cindent
set cinoptions=g0,:0,N-s,(0
set tabstop=3
set shiftwidth=3
set foldmethod=manual

"++++++ Chemes ++++++++++++++++
set list
set listchars=eol:\ ,tab:\ \|\ ,space:\ 
hi CursorLine ctermbg=NONE ctermfg=NONE 
set t_Co=256
hi Normal ctermbg=233 guibg=NONE ctermfg=252
colorscheme slate

"++++++ Path ++++++++++++++++++
let $DICT="$VIM/vplug/vim-dict/dict"
let $LR="~/.vimrc"
let $WR="$VIM/_vimrc"
let $VU="$VIM/.."
let $VUU="$VIM/../.."
let $C="$HOME/Desktop"
let $VD="$VIM/vdoc"

"++++++ Vim-Plug ++++++++++++++
silent! call plug#begin('$VIM/vplug')
Plug '$VIM/vplug/emmet',{'for':['html','htm','css','js']}
Plug '$VIM/vplug/vim-airline'
	let g:user_emmet_expandabbr_key='<S-Tab>'
Plug '$VIM/vplug/nerdtree'
	map I :NERDTreeToggle<CR>
	map! I :NERDTreeToggle<CR>
	map <A-S-I> :NERDTreeToggle<CR>
	map! <A-S-I> :NERDTreeToggle<CR>
Plug '$VIM/vplug/auto-pairs'
Plug '$VIM/vplug/undotree'
	map U :UndotreeToggle<CR>
	map! U :UndotreeToggle<CR>
	map <A-S-U> :UndotreeToggle<CR>
	map! <A-S-U> :UndotreeToggle<CR>
Plug '$VIM/vplug/vim-autocomplete'
	let g:ftplugin_sql_omni_key_right = '<leader><RIGHT>'
	let g:ftplugin_sql_omni_key_left = '<leader><LEFT>'
Plug '$VIM/vplug/vim-dict'
Plug '$VIM/vplug/vimcdoc'
Plug '$VIM/vplug/nerdcommenter'
	let g:NERDSpaceDelims=1
Plug '$VIM/vplug/vim-visual-multi'
Plug '$VIM/vplug/vim-surround'
Plug '$VIM/vplug/vim-wildfire'
Plug '$VIM/vplug/img-paste'
	autocmd FileType markdown nmap <buffer><silent> <leader>p :call mdip#MarkdownClipboardImage()<CR>
	let g:mdip_imgdir = 'assets'
	let g:mdip_imgname = 'image'
call plug#end()

"++++++ Keys ++++++++++++++++++
let mapleader=";"
"set timeoutlen=400
"++++++ Move ++++++++++++++++++
imap <leader>h <LEFT>
imap <leader>j <DOWN>
imap <leader>k <UP>
imap <leader>l <RIGHT>
nmap <C-H> <LEFT>
nmap <C-J> <DOWN>
nmap <C-K> <UP>
nmap <C-L> <RIGHT>

"++++++ EditCode ++++++++++++++
imap <leader>' <++>
nmap <leader>' i<++><ESC>F<
imap <leader>f <ESC>/<++><CR>ca<
nmap <leader>f /<++><CR>ca<
imap <leader>F <ESC>gg/<++><CR>ca<
nmap <leader>F gg/<++><CR>ca<

"++++++ Edit ++++++++++++++++++
imap <leader>a <ESC>A
imap <leader>i <ESC>I
imap <leader>y <ESC>yy
imap <leader>u <ESC>ua
imap <leader>p <ESC>pi
imap <leader>v <ESC><RIGHT>v
imap <leader>V <ESC>V
imap <leader>x <Del>
imap <leader>o <ESC>o
imap <leader>O <ESC>O
nmap <leader>o o<ESC>k
nmap <leader>O O<ESC>j
imap <leader><leader><CR> <ESC>O
imap <leader>" <ESC>"
imap <leader><leader>" <ESC>"*

"++++++ Table +++++++++++++++++
imap <leader><leader>1 \|<SPACE><++><SPACE>\|<ESC>
imap <leader><leader>2 \|<ESC>2a<SPACE><++><SPACE>\|<ESC>
imap <leader><leader>3 \|<ESC>3a<SPACE><++><SPACE>\|<ESC>
imap <leader><leader>4 \|<ESC>4a<SPACE><++><SPACE>\|<ESC>
imap <leader><leader>5 \|<ESC>5a<SPACE><++><SPACE>\|<ESC>
imap <leader><leader>6 \|<ESC>6a<SPACE><++><SPACE>\|<ESC>
imap <leader><leader>7 \|<ESC>7a<SPACE><++><SPACE>\|<ESC>
imap <leader><leader>8 \|<ESC>8a<SPACE><++><SPACE>\|<ESC>
imap <leader><leader>9 \|<ESC>9a<SPACE><++><SPACE>\|<ESC>
nmap <leader><leader>1 o\|<SPACE><++><SPACE>\|<ESC>
nmap <leader><leader>2 o\|<ESC>2a<SPACE><++><SPACE>\|<ESC>
nmap <leader><leader>3 o\|<ESC>3a<SPACE><++><SPACE>\|<ESC>
nmap <leader><leader>4 o\|<ESC>4a<SPACE><++><SPACE>\|<ESC>
nmap <leader><leader>5 o\|<ESC>5a<SPACE><++><SPACE>\|<ESC>
nmap <leader><leader>6 o\|<ESC>6a<SPACE><++><SPACE>\|<ESC>
nmap <leader><leader>7 o\|<ESC>7a<SPACE><++><SPACE>\|<ESC>
nmap <leader><leader>8 o\|<ESC>8a<SPACE><++><SPACE>\|<ESC>
nmap <leader><leader>9 o\|<ESC>9a<SPACE><++><SPACE>\|<ESC>
imap <leader><leader>: <ESC>gg/<++><CR>ca<:--:
nmap <leader><leader>: gg/<++><CR>ca<:--:<ESC>
imap <leader>ta <table><CR><tr><CR><TAB><td<SPACE>colspan="<++>"><++></td><CR><BackSpace></tr><CR><table>
nmap <leader>ta o<table><CR><tr><CR><TAB><td<SPACE>colspan="<++>"><++></td><CR><BackSpace></tr><CR></table>
imap <leader>tr <tr><CR><TAB><td<SPACE>colspan="<++>"><++></td><CR><BackSpace></tr>
nmap <leader>tr o<tr><CR><TAB><td<SPACE>colspan="<++>"><++></td><CR><BackSpace></tr>

"++++++ MarkDown ++++++++++++++
imap <leader>m <mark></mark><LEFT><LEFT><LEFT><LEFT><LEFT><LEFT><LEFT>

"++++++ EXMode ++++++++++++++++
imap <leader><leader>w <ESC>:w<CR>
nmap <leader><leader>w :w<CR>
imap <leader><leader>r <ESC>:reg<CR>
nmap <leader><leader>r :reg<CR>
imap <leader><leader>d <ESC>:set<SPACE>dictionary+=$DICT/.dic<LEFT><LEFT><LEFT><LEFT>
nmap <leader><leader>d :set<SPACE>dictionary+=$DICT/.dic<LEFT><LEFT><LEFT><LEFT>
imap <leader><leader>D <ESC>:set<SPACE>dictionary-=$DICT/.dic<LEFT><LEFT><LEFT><LEFT>
nmap <leader><leader>D :set<SPACE>dictionary-=$DICT/.dic<LEFT><LEFT><LEFT><LEFT>
imap <leader><leader>f <ESC>:set<SPACE>fenc=
nmap <leader><leader>f :set<SPACE>fenc=
imap <leader><leader>u <ESC>:set<SPACE>fenc=utf-8<CR>
nmap <leader><leader>u :set<SPACE>fenc=utf-8<CR>
imap <leader><leader>c <ESC>:set<SPACE>fenc=cp936<CR>
nmap <leader><leader>c :set<SPACE>fenc=cp936<CR>
imap <leader><leader>n <ESC>:set<SPACE>nohlsearch<CR>
nmap <leader><leader>n :set<SPACE>nohlsearch<CR>
imap <leader><leader>p <ESC>:set<SPACE>paste<CR>
nmap <leader><leader>p :set<SPACE>paste<CR>
imap <leader><leader>P <ESC>:set<SPACE>nopaste<CR>
nmap <leader><leader>P :set<SPACE>nopaste<CR>

"++++++ File Header ++++++++++++
autocmd BufNewFile *.py,*.cpp,*.[ch],*.sh,*.c exec ":call SetTitle()" 
func SetTitle() 
    if &filetype == 'sh' 
        call setline(1,"#!/bin/bash") 
	endif
	if &filetype == 'python'
		call setline(1,"#!/usr/bin/python")
		call setline(2,"#coding:utf-8")
    endif
    if &filetype == 'cpp'
		call setline(1,"#include<iostream>")
		call setline(2,"using namespace std;")
    endif
    if &filetype == 'c'
		call setline(1,"#include<stdio.h>")
    endif
    autocmd BufNewFile * normal G
endfunc 
