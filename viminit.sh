#!/bin/bash
sudo mkdir ~/.vim/view -p
sudo cp ~/git/vim/_vimrc ~/.vimrc
sudo cp -r ~/git/vim/vplug/plug.vim /usr/share/vim/vim*/autoload/
sudo cp -r ~/git/vim/vplug /usr/share/vim/
sudo cp -r ~/git/vim/.vim /usr/share/vim/
